#ifndef AkmECompass_H
#define AkmECompass_H

#include "mbed.h"

#define AKMECOMPASS_LEN_ONE_BYTE        1
#define AKMECOMPASS_LEN_BUF_MAX					4

/**
 * Collection class for handling commands to all AKM E-Compass modules.
 *
 * 3-Axis Electronic Compass Devices: AK8963, AK09911, AK09912, AK09915, 
 * AK09916C, AK09916D, AK09917, AK09918, AK09940
 */
class AkmECompass
{
public:
    /** 
     * Possible slave address of AkmECompass. Selected by CAD1 and CAD0 pins. 
     */
    typedef enum {
        SLAVE_ADDR_1 = 0x0c,    /**< CAD0 = Low, CAD1 = Low */
        SLAVE_ADDR_2 = 0x0d,    /**< CAD0 = High, CAD1 = Low */
        SLAVE_ADDR_3 = 0x0e,    /**< CAD0 = Low, CAD1 = High */
        SLAVE_ADDR_4 = 0x0f,    /**< CAD0 = High, CAD1 = High */
    } SlaveAddress;
    
    
    /** 
     * Device ID stored in the register(WIA2). 
     */
    typedef enum {
        AK8963   = 0x00,
        AK09911  = 0x05,       
        AK09912  = 0x04,       
        AK09915  = 0x10,       
        AK09916C = 0x09,       
        AK09916D = 0x0B,       
        AK09918  = 0x0C,       
        AK09917  = 0x0D,       
        AK09940  = 0xA1,       
    } DeviceId;
    
    /**
     * Available opration modes in AkmECompass.
     */
    typedef enum {
        MODE_POWER_DOWN           = 0x00,       /**< Power-down mode */
        MODE_SINGLE_MEASUREMENT   = 0x01,       /**< Single measurement mode */
        MODE_AK8963_CONTINUOUS_1  = 0x02,       /**< Continuous measurement mode 1, 8 Hz */
        MODE_AK8963_CONTINUOUS_2  = 0x06,       /**< Continuous measurement mode 2, 100 Hz */
        MODE_AK8963_SELF_TEST     = 0x08,       /**< Self test mode */
        MODE_AK099XX_CONTINUOUS_1 = 0x02,       /**< Continuous measurement mode 1, 10 Hz */
        MODE_AK099XX_CONTINUOUS_2 = 0x04,       /**< Continuous measurement mode 2, 20 Hz */
        MODE_AK099XX_CONTINUOUS_3 = 0x06,       /**< Continuous measurement mode 3, 50 Hz */
        MODE_AK099XX_CONTINUOUS_4 = 0x08,       /**< Continuous measurement mode 4, 100 Hz */
        MODE_AK099XX_CONTINUOUS_5 = 0x0A,       /**< Continuous measurement mode 5, 200 Hz (Only AK09915,AK09917) */
        MODE_AK099XX_CONTINUOUS_6 = 0x0C,       /**< Continuous measurement mode 6, 1/400 Hz (Only AK09915,AK09917,AK09940) */
        MODE_AK099XX_SELF_TEST    = 0x10,       /**< Self test mode */
        MODE_SINGLE_LOOP          = 0x11,       /**< Looping single measurement(Special feature for AKDP) */
    } OperationMode;

    /**
     * Status of function. 
     */
    typedef enum {
        SUCCESS,               /**< The function processed successfully. */
        ERROR_I2C_READ,        /**< Error related to I2C read. */
        ERROR_I2C_WRITE,       /**< Error related to I2C write. */
        ERROR,                 /**< General Error */
        DATA_READY,            /**< Data ready */
        NOT_DATA_READY,        /**< Data ready is not asserted. */
        DATA_OVER_RUN,         /**< Data over run. */
    } Status;
    
    
    typedef struct {
        OperationMode mode;
        uint8_t options[AKMECOMPASS_LEN_BUF_MAX];
    } Mode;
    
        /**
     * Structure to hold a magnetic vector in LSB.
     */
    typedef struct {
        int32_t lsbX;    /**< X component */
        int32_t lsbY;    /**< Y component */
        int32_t lsbZ;    /**< Z component */
        int8_t lsbTemp;  /**< temperature */
        bool isOverflow; /**< Flag for magnetic sensor overflow */
    } MagneticVectorLsb;
    
    /**
     * Structure to hold a magnetic vector.
     */
    typedef struct {
        float mx;        /**< X component in uT */
        float my;        /**< Y component in uT */
        float mz;        /**< Z component in uT */
        float temp;      /**< temperature in degC */
        bool isOverflow; /**< Flag for magnetic sensor overflow */
    } MagneticVector;

    /**
     * Sensitivity adjustment data.
     */
    typedef struct {
        unsigned char x;   /**< sensitivity adjustment value for x */
        unsigned char y;   /**< sensitivity adjustment value for y */
        unsigned char z;   /**< sensitivity adjustment value for z */
    } SensitivityAdjustment;
    
    /**
     * Constructor.
     *
     */
    AkmECompass(){}

    /**
     * Destructor.
     *
     */
    virtual ~AkmECompass(){}

    /**
     * Initialize sensor with I2C connection.
     *
     * @param _i2c Pointer to the I2C instance
     * @param addr Slave address of the device
     * @param deviceId Device Id enum
     *
     */
    virtual void init(I2C *_i2c, SlaveAddress addr, DeviceId deviceId) = 0;

    /**
     * Initialize sensor with SPI connection.
     *
     * @param _spi Pointer to the SPI instance
     * @param _cs Pointer to the chip select DigitalOut pin
     * @param deviceId Device Id enum
     *
     */
    virtual void init(SPI *_spi, DigitalOut *_cs, DeviceId deviceId) = 0;
    
    /**
     * Check the connection. 
     *
     * @note Connection check is performed by reading a register which has a fixed value and verify it.
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual Status checkConnection() = 0;

    /**
     * Gets device operation mode.
     *
     * @param mode device opration mode
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual Status getOperationMode(Mode *mode) = 0;

    /**
     * Sets device operation mode.
     *
     * @param mode device opration mode
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual Status setOperationMode(Mode mode) = 0;

    /**
     * Check if data is ready, i.e. measurement is finished.
     *
     * @return Returns DATA_READY if data is ready or NOT_DATA_READY if data is not ready. If error happens, returns another code.
     */
    virtual Status isDataReady() = 0;

    /**
     * Gets magnetic vector from the device in uT.
     *
     * @param vec Pointer to a instance of MagneticVector
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual Status getMagneticVector(MagneticVector *vec) = 0;
    
    /**
     * Gets magnetic vector from the device in LSB.
     *
     * @param lsb Pointer to a instance of MagneticVectorLsb
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual Status getMagneticVectorLsb(MagneticVectorLsb *lsb) = 0;
    
    /** 
     * Writes data to the device. 
     * 
     * @param registerAddress register address to be written
     * @param buf data to be written
     * @param length of the data
     * 
     * @return Returns SUCCESS when succeeded, otherwise returns another.
     */
    Status write(char registerAddress, const char *buf, int length){

      char data[AKMECOMPASS_LEN_BUF_MAX];

      if(i2c != NULL){
        data[0] = registerAddress;
        for (int i=0; i < length; i++)
            data[1+i] = buf[i];

        // Writes a start address. Doesn't send a stop condition.
          if (i2c->write((slaveAddress << 1), data, length + 1) != 0)
              return AkmECompass::ERROR_I2C_WRITE;

      }
      else if(spi != NULL){
        spi->lock();
        for(int i = 0; i < length; i++)
        {
          data[0] = registerAddress+i;
          data[1] = buf[i];
          cs->write(0);
          spi->write(data, 2, 0, 0);
          cs->write(1);
        }
        spi->unlock();
      }
      else{
          return AkmECompass::ERROR;
      }
      return AkmECompass::SUCCESS;
    }

    /** 
     * Reads data from device. 
     * 
     * @param registerAddress register address to be read
     * @param buf buffer to store the read data
     * @param length bytes to be read
     * 
     * @return Returns SUCCESS when succeeded, otherwise returns another.
     */
    Status read(char registerAddress, char *buf, int length){            

    	if(i2c != NULL){

            // Writes a start address
            if (i2c->write((slaveAddress << 1), &registerAddress, AKMECOMPASS_LEN_ONE_BYTE) != 0)
                return AkmECompass::ERROR_I2C_WRITE;
            
            // Reads register data
            if (i2c->read((slaveAddress << 1), buf, length) != 0)
                return AkmECompass::ERROR_I2C_READ;

        }
        else if(spi != NULL){
            char tx_buf = registerAddress | 0x80;
            spi->lock();
            cs->write(0);
            spi->write(&tx_buf, 1, 0, 0);
            spi->write(0,0,buf,length);
            cs->write(1);
            spi->unlock();
        }
        else{
            return AkmECompass::ERROR;        
        }

        return AkmECompass::SUCCESS;
    }
    
protected:
    I2C *i2c;					/**< Holds a pointer to an I2C object. */
    SPI *spi;					/**< Holds a pointer to an SPI object. */
    DigitalOut *cs;				/**< Holds a DigitalOut oblject for CS. */
    SlaveAddress slaveAddress;	/**< Holds the slave address of AkmECompass. */
    DeviceId deviceId;			/**< Holds the device ID(WIA2). */
    
};

#endif
